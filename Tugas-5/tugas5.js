// soal 1
function halo(){
	return "Halo Sanbers!";
} 

console.log("********* Output Soal 1 ***********");
console.log(halo());
console.log("********* ------------- ***********\n");

// soal 2
function kalikan(num1, num2){
	if (isNaN(num1) || isNaN(num2))
		return "input bukan angka";
	return Number(num1) * Number(num2);
}

var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);

console.log("********* Output Soal 2 ***********");
console.log(hasilKali);
console.log("********* ------------- ***********\n");

// soal 3
function introduce(name, age, address, hobby){
	return "Nama saya "+ name +", umur saya "+ age +" tahun, alamat saya di "+ address +", dan saya punya hobby yaitu "+ hobby +"!";
}
 
var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);

console.log("********* Output Soal 3 ***********");
console.log(perkenalan);
console.log("********* ------------- ***********\n");

// soal 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992];
var person = {
	name : arrayDaftarPeserta[0],
	gender : arrayDaftarPeserta[1], 
	hobby : arrayDaftarPeserta[2],
	dob : arrayDaftarPeserta[3]
}

console.log("********* Output Soal 4 ***********");
console.log(person);
console.log("********* ------------- ***********\n");

// soal 5
var fruits = [
	{
		nama: "strawberry",
		warna: "merah",
		adaBijinya: "tidak",
		harga: 9000 
	},
	{
		nama: "jeruk",
		warna: "oranye",
		adaBijinya: "ada",
		harga: 8000 
	},
	{
		nama: "Semangka",
		warna: "Hijau & Merah",
		adaBijinya: "ada",
		harga: 10000 
	},
	{
		nama: "Pisang",
		warna: "Kuning",
		adaBijinya: "tidak",
		harga: 5000 
	},
];

console.log("********* Output Soal 5 ***********");
console.log(fruits[0]);
console.log("********* ------------- ***********\n");

// soal 6
var dataFilm = [];

function addFilm(nama, durasi, genre, tahun){
	dataFilm.push({
		nama : nama,
		durasi : durasi,
		genre : genre,
		tahun : tahun
	});
}

addFilm("Ada Hantu", 20, "Romance", 2009);
addFilm("Ngoding Buat Kamu", 120, "Romance", 2019);


console.log("********* Output Soal 6 ***********");
console.log(dataFilm);
console.log("********* ------------- ***********\n");
