// soal 1
console.log("********* Output Soal 1 ***********");

var nilai = 0;

console.log("LOOPING PERTAMA")
while (nilai < 20) {
	nilai += 2;
	console.log(nilai + " - I love coding");
}

console.log("LOOPING KEDUA")
while (nilai >= 2) {
	console.log(nilai + " - I will become a frontend developer");
	nilai -= 2;
}
console.log("********* ------------- ***********\n");




// soal 2
console.log("********* Output Soal 2 ***********");
for (var angka = 1; angka <= 20; angka++) {
	if (angka % 3 === 0 && angka % 2 === 1) {
		console.log(angka + " I Love Coding");
	} else if (angka % 2 === 0) {
		console.log(angka + " Berkualitas");
	} else if (angka % 2 === 1) {
		console.log(angka + " Santai");
	}
}
console.log("********* ------------- ***********\n");




// soal 3
console.log("********* Output Soal 3 ***********");
for (var index = 1; index <= 7; index++) {
	var rowOutput = "";
	for (var i = 0; i < index; i++) {
		rowOutput += "#";
	}
	console.log(rowOutput);
}
console.log("********* ------------- ***********\n");




// soal 4
console.log("********* Output Soal 4 ***********");

var kalimat="saya sangat senang belajar javascript"

var arrKalimat = kalimat.split(" ");

console.log(arrKalimat);

console.log("********* ------------- ***********\n");




// soal 5
console.log("********* Output Soal 5 ***********");

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];

/** cara 1 */
var sortDescDaftarBuah = daftarBuah.sort().reverse();

while (sortDescDaftarBuah.length > 0) {
	console.log(sortDescDaftarBuah.pop());
}

console.log("********* ------------- ***********\n");