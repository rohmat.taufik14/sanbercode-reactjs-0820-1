// soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var kataGabungan = kataPertama.concat( 
	" " + kataKedua.charAt(0).toUpperCase() + kataKedua.substr(1, kataKedua.length -1), 
	" " + kataKetiga, 
	" " + kataKeempat.toUpperCase());

console.log("********* Output Soal 1 ***********");
console.log(kataGabungan);
console.log("\n");

// soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var jumlah = parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat);

console.log("********* Output Soal 2 ***********");
console.log(jumlah);
console.log("\n");

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log("********* Output Soal 3 ***********");
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
console.log("\n");


// soal 4
var nilai = 4;
var indeks = null;

if (nilai >= 80) { indeks = 'A' }
else if (nilai >= 70 && nilai < 80) { indeks = 'B' }
else if (nilai >= 60 && nilai < 70) { indeks = 'c' }
else if (nilai >= 50 && nilai < 60) { indeks = 'D' }
else { indeks = 'E' }

console.log("********* Output Soal 4 ***********");
console.log("Nilai " + nilai + " berindeks " + indeks);
console.log("\n");

// soal 5
var tanggal = 14;
var bulan = 2;
var tahun = 1996;

switch (bulan){
	case 1: { bulan = "Januari"; break; }
	case 2: { bulan = "Februari"; break; }
	case 3: { bulan = "Maret"; break; }
	case 4: { bulan = "April"; break; }
	case 5: { bulan = "Mei"; break; } 
	case 6: { bulan = "Juni"; break; }
	case 7: { bulan = "Juli"; break; }
	case 8: { bulan = "Agustus"; break; }
	case 9: { bulan = "September"; break; }
	case 10: { bulan = "Oktober"; break; }
	case 11: { bulan = "November"; break; }
	case 12: { bulan = "Desember"; break; }
	default: { bulan = "Tidak terdefinisi" }
}

console.log("********* Output Soal 5 ***********");
console.log("Tanggal lahir : " + tanggal + " " + bulan + " " + tahun);
console.log("\n");



