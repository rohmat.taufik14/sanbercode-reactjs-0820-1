// soal 1
const phi = 3.14;

const hitungLuasLingkaran = (r) => 0.5 * phi * r * r;

const hitungKelilingLingkaran = (r) => 2 * phi * r;

let r = 1;
console.log("********* Output Soal 1 ***********");
for (let i = 0; i < 10; i+=2) {
	console.log("r = " + r + ", L = " + hitungLuasLingkaran(r) + ", K = " + hitungKelilingLingkaran(r));
	r += 2;
}
console.log("********* ------------- ***********\n");


// soal 2
const gabungKata = (kataPertama, kataKedua) => {
	if (kataPertama === '' || kataKedua === '')
		return `${kataPertama}${kataKedua}`;
	else
		return `${kataPertama} ${kataKedua}`;
}

let kalimat = "";

kalimat = gabungKata(kalimat, 'saya');
kalimat = gabungKata(kalimat, 'adalah');
kalimat = gabungKata(kalimat, 'seorang');
kalimat = gabungKata(kalimat, 'frontend');
kalimat = gabungKata(kalimat, 'developer');

console.log("********* Output Soal 2 ***********");
console.log(kalimat);
console.log("********* ------------- ***********\n");


// soal 3
const newFunction = (firstName, lastName) => (
  	{
    	firstName: firstName,
    	lastName: lastName,
    	fullName: `${firstName} ${lastName}`
  	}
);

console.log("********* Output Soal 3 ***********");
console.log(newFunction("William", "Imoh").fullName);
console.log("********* ------------- ***********\n");


// soal 4
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation} = newObject;

console.log("********* Output Soal 4 ***********");
console.log(firstName, lastName, destination, occupation);
console.log("********* ------------- ***********\n");


// soal 5
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

console.log("********* Output Soal 5 ***********");
console.log(combined);
console.log("********* ------------- ***********\n");

