// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let [time, index] = [10000, 0];
function doReadBook(){
	readBooks(time, books[index], function(sisaWaktu){
		if (books.length > index && sisaWaktu > 0) {
			time = sisaWaktu;
			index += 1;
			doReadBook();
		}
	});
}

doReadBook();
