var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let [time, index] = [10000, 0];
function doReadBook(){
	readBooksPromise(time, books[index])
		.then(function(sisaWaktu){
			time = sisaWaktu;
			index += 1;
			if (books.length > index && sisaWaktu > 0){
				doReadBook();
			}
		})
		.catch(function(error){
			console.log(error.message);
		});
}

doReadBook();